﻿using System.Text;

namespace Alfasoft
{
    public class Api
    {
        HttpClient client;
        StringBuilder users = new StringBuilder(); 

        public string getUsers()
        {
            return users.ToString(); //retorna todas os dados das consultas realizadas.
        }
            
        public void getUsersApi(string uuid)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://api.bitbucket.org/2.0/users/" + uuid);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(client.BaseAddress).Result; //realiza a requisição para a url informada.
            if (response.IsSuccessStatusCode) //verifica se a consulta foi realizada com sucesso.
            {
                var user = response.Content.ReadAsStringAsync().Result; //lê os dados retornados.
                users.Append(user+"\n"); //guarda o retorno na variável.
                Console.WriteLine("\nO usuário que possui o seguinte UUID está sendo recuperado: " + uuid + "\nA URL utilizada é: " + "https://api.bitbucket.org/2.0/users/" + uuid);
            }
            else
                Console.WriteLine("Error: " + response.StatusCode);
        }        
    }
}
