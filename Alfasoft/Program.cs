﻿using Alfasoft;
try
{
    var arq = new FileInfo(Directory.GetCurrentDirectory() + "\\logAccess.txt"); //Tenta recuperar um arquivo que servirá de referência sobre quando o programa foi executado pela última vez.
    if(arq.Exists)
    {
        var time = arq.LastAccessTime;
        var timeDiff = DateTime.Now.Subtract(time);        
        if (timeDiff.Days == 0 && timeDiff.Hours == 0 && timeDiff.Minutes == 0 && timeDiff.Seconds < 60) //Verifica se o programa foi executado pela última vez há menos de 60 segundos e faz com que o usuário aguarde o tempo restante.
        {
            Console.WriteLine("O programa foi executado há menos de 60 segundos, aguarde "+(60-timeDiff.Seconds)+ " segundos para iniciar a execução...");
            Thread.Sleep((60 - timeDiff.Seconds) * 1000);
        }      
    }
    File.Create("logAccess.txt"); //Cria o arquivo
    var fullPath = Console.ReadLine(); //Lê o caminho do arquivo.
    var archiveApi = new FileInfo(fullPath);
    while (!archiveApi.Exists) //Enquanto o arquivo não existir, segue tentando ler um arquivo que exista.
    {
        Console.WriteLine("Arquivo inexistente, tente outro caminho!");
        fullPath = Console.ReadLine();
        archiveApi = new FileInfo(fullPath);
    }
    Console.WriteLine("Lendo arquivo e consumindo API...");
    var archive = new StreamReader(fullPath);
    string data;
    var api = new Api();
    StreamWriter writer = new StreamWriter(Directory.GetCurrentDirectory() + "\\logAPI.txt"); //Cria ponteiro para escrever no arquivo de log.
    while ((data = archive.ReadLine()) != null) //lê cada linha do arquivo.
    {
        api.getUsersApi(data); //chama o método getUsersApi passando o uuid por parâmetro.
        Thread.Sleep(5000); //Aguarda cinco segundos para próxima requisição.
    }
    writer.WriteLine(api.getUsers()); //escreve no arquivo de log o retorno da API.
    writer.Close(); //fecha o arquivo.    
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
Thread.Sleep(5000); //Aguarda cinco segundos antes da finalização do programa.
